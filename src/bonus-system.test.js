import istanbul from "istanbul";
import { calculateBonuses } from "./bonus-system";

describe('Bonus system', () => { 

it('Programs work correctly', () => {
    expect(calculateBonuses('Standard', 1)).toBe(0.05 * 1);
    expect(calculateBonuses('Standard', 10000)).toBe(0.05 * 1.5);
    expect(calculateBonuses('Standard', 50000)).toBe(0.05 * 2);
    expect(calculateBonuses('Standard', 100000)).toBe(0.05 * 2.5);

    expect(calculateBonuses('Premium', 1)).toBe(0.1 * 1);
    expect(calculateBonuses('Premium', 10000)).toBe(0.1 * 1.5);
    expect(calculateBonuses('Premium', 50000)).toBe(0.1 * 2);
    expect(calculateBonuses('Premium', 100000)).toBe(0.1 * 2.5);

    expect(calculateBonuses('Diamond', 1)).toBe(0.2 * 1);
    expect(calculateBonuses('Diamond', 10000)).toBe(0.2 * 1.5);
    expect(calculateBonuses('Diamond', 50000)).toBe(0.2 * 2);
    expect(calculateBonuses('Diamond', 100000)).toBe(0.2 * 2.5);
    });

it('Handles program names correctly', () => {
    expect(calculateBonuses('NotAProgram', 1000)).toBe(0.0);
    expect(calculateBonuses('10', 1000)).toBe(0);
    });

});